<div class="l-page">
	<header class="l-header-wrap wrapper" role="banner">
		<div class="l-container">
			<div class="l-branding">
		        <?php if ($logo): ?>
		          <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" class="site-branding__logo"><img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" /></a>
		        <?php endif; ?>
		        
			</div>					

			<div class="l-header">
					<?php if (!empty($page['header'])): ?> 
					<?php print render($page['header']) ?>
					<?php endif; ?>
					
			</div><!-- end .l-header -->
	
			<navigation class="l-navigation" role="navigation">
					<?php if (!empty($page['navigation'])): ?>
					<?php print render($page['navigation']); ?>
					<?php endif; ?>
			</navigation>
			
			
		</div><!-- end .l-container -->
		
	</header> <!-- end .l-header-wrap -->
	
	<div class="l-titlebar-wrap wrapper">
		<div class="l-container">
			<div class="l-titlebar">
				
				
				<div class="l-ctabox">
					<?php if (!empty($page['ctabox'])): ?>
					<?php print render($page['ctabox']); ?>
					<?php endif; ?>
				</div><!-- end .l-ctabox -->

				<div class="l-titlebox">
					<!-- If content in titlebox render it, otherwise render page title -->
					<?php if (!empty($page['titlebox'])): ?>
					<?php print render($page['titlebox']); ?>
					<?php elseif ($title): ?>
						<h1 class="page-title default-page-title"><?php print $title; ?></h1>
						<?php // if ($node->body['summary']) ?>
						<?php // print $node->body['summary']; ?>
						<?php // endif; ?>
					<?php endif; ?>				
				</div><!-- end .l-titlebox -->
						
			</div><!-- end .l-titlebar -->
		</div><!-- end .l-container -->
	</div><!-- end .l-titlebar-wrap -->
	
	<div class="l-main-wrap wrapper">
		<div class="l-container">		
			<div class="l-content">
			<?php print render($tabs); ?>
			<?php //print $breadcrumb; ?>
			<?php print $messages; ?>
			<?php print render($page['help']); ?>
			
			<!-- Action Links -->
			<?php if ($action_links): ?>
		    <ul class="action-links"><?php print render($action_links); ?></ul>
			<?php endif; ?>
			
			<?php print render($page['content']); ?>
			
			</div><!-- end .l-content -->
			
			<div class="l-sidebar-first">
			
				<?php print render($page['sidebar_first']); ?>
			
			</div><!-- end .l-sidebar-first -->
		</div><!-- end .l-container -->
	</div><!-- end .l-main-wrap -->
	
</div>

<div class="l-footer-prefix-wrap wrapper">
		<div class="l-container">		
			<div class="l-footer-prefix">
				<div class="l-footer-prefix-first">
						<?php if (!empty($page['footer_prefix_first'])): ?>
						<?php print render($page['footer_prefix_first']); ?>
						<?php endif; ?>
				</div><!-- end .l-footer-prefix-first -->
				<div class="l-footer-prefix-second">
						<?php if (!empty($page['footer_prefix_second'])): ?>
						<?php print render($page['footer_prefix_second']); ?>
						<?php endif; ?>
				</div><!-- end .l-footer-prefix-second -->
				<div class="l-footer-prefix-third">
						<?php if (!empty($page['footer_prefix_third'])): ?>
						<?php print render($page['footer_prefix_third']); ?>
						<?php endif; ?>
				</div><!-- end .l-footer-prefix-third -->
			</div><!-- end .l-footer -->
		</div><!-- end .l-container -->
</div><!-- end .l-footer-wrap -->


<footer class="l-footer-wrap wrapper" role="contentinfo">
		<div class="l-container">		
			<div class="l-footer">
				<div class="l-footer-first">
						<?php if (!empty($page['footer_first'])): ?>
						<?php print render($page['footer_first']); ?>
						<?php endif; ?>
				</div><!-- end .l-footer-first -->
				<div class="l-footer-second">
						<?php if (!empty($page['footer_second'])): ?>
						<?php print render($page['footer_second']); ?>
						<?php endif; ?>
				</div><!-- end .l-footer-second -->
			</div><!-- end .l-footer -->
		</div><!-- end .l-container -->
</footer><!-- end .l-footer-wrap -->
</div><!-- end .l-page -->
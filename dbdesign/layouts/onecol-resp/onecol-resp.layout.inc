name = One Column Responsive Layout
description = A 12 column Singularity GS layout with a header, titlebar, two-column content region, 3 column footer prefix and 2 column footer.
preview = preview.png
template = onecol-resp-layout

; Regions
regions[navigation]     = Navigation bar
regions[header]         = Header
regions[branding]       = Branding
regions[titlebox]    	= Title Box
regions[ctabox]    	= CTA Box
regions[help]           = Help
regions[content]        = Content
regions[footer_prefix_first]   = Footer Prefix First
regions[footer_prefix_second]  = Footer Prefix Second
regions[footer_prefix_third]   = Footer Prefix Third
regions[footer_first]          = Footer First
regions[footer_second]         = Footer Second

; Stylesheets
stylesheets[all][] = css/layouts/onecol-resp/onecol-resp-layout.css
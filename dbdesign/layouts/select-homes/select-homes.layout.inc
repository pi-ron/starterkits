name = Select Homes Responsive Layout
description = Same as Two Column Responsive but with revised branding styling.
preview = preview.png
template = select-homes-layout

; Regions
regions[navigation]     = Navigation bar
regions[header]         = Header
regions[branding]       = Branding
regions[titlebox]    	= Title Box
regions[titlebox_left]    	= Title Box Left
regions[titlebox_right]    	= Title Box Right
regions[ctabox]    	= CTA Box
regions[help]           = Help
regions[content]        = Content
regions[sidebar_first]  = First sidebar
regions[footer_prefix_first]   = Footer Prefix First
regions[footer_prefix_second]  = Footer Prefix Second
regions[footer_prefix_third]   = Footer Prefix Third
regions[footer_first]          = Footer First
regions[footer_second]         = Footer Second

; Stylesheets
stylesheets[all][] = css/layouts/select-homes/select-homes-layout.css
name = Two Column Responsive Basic Layout
description = A Singularity GS responsive layout with a branding, header, titlebar, 3 column content prefix, two-column main content and 3 column footer.
preview = preview.png
template = twocol-resp-basic-layout

; Regions
regions[branding]       = Branding
regions[header]         = Header
regions[navigation]     = Navigation bar
regions[banner]     	= Banner
regions[titlebox]    	= Title Box
regions[help]           = Help
regions[prefix_first] 	= Prefix First
regions[prefix_second] 	= Prefix First
regions[prefix_third] 	= Prefix First
regions[content]        = Content
regions[sidebar_first]  = First sidebar
regions[footer_first]   = Footer First
regions[footer_second]  = Footer Second
regions[footer_third]   = Footer Third

; Stylesheets
stylesheets[all][] = css/layouts/twocol-resp-basic/twocol-resp-basic-layout.css
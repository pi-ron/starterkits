<div class="l-page">


<!-- L-HEADER ============================================== -->

	<header class="l-header-wrap wrapper" role="banner">
		<div class="l-container">
			
			<!-- L-BRANDING ============== -->
			<div class="l-branding">
		        <?php if ($logo): ?>
		          <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" class="site-branding__logo"><img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" /></a>
		        <?php endif; ?>
		        
			</div>					
			
			<!-- L-HEADER ============== -->
			<div class="l-header">
					<?php if (!empty($page['header'])): ?> 
					<?php print render($page['header']) ?>
					<?php endif; ?>
					
			</div><!-- end .l-header -->
		</div><!-- end .l-container -->	
		
	</header> <!-- end .l-header-wrap -->

<!-- L-NAVIGATION ============================================== -->

	<div class="l-container">
			
		<!-- L-NAVIGATION ============== -->
		<navigation class="l-navigation" role="navigation">
				<?php if (!empty($page['navigation'])): ?>
				<?php print render($page['navigation']); ?>
				<?php endif; ?>
		</navigation>	
		
	</div><!-- end .l-container -->

	
<!-- L-TITLEBAR ============================================== -->

	<div class="l-titlebar-wrap wrapper">
		<div class="l-container">
			<div class="l-titlebar">
			
				<!-- L-TITLEBOX ============== -->
				<div class="l-titlebox">
					<!-- If content in titlebox render it, otherwise render page title -->
					<?php if (!empty($page['titlebox'])): ?>
					<?php print render($page['titlebox']); ?>
					<?php elseif ($title): ?>
						<h1 class="page-title default-page-title"><?php print $title; ?></h1>
						<?php // if ($node->body['summary']) ?>
						<?php // print $node->body['summary']; ?>
						<?php // endif; ?>
					<?php endif; ?>				
				</div><!-- end .l-titlebox -->
						
			</div><!-- end .l-titlebar -->
		</div><!-- end .l-container -->
	</div><!-- end .l-titlebar-wrap -->

<!-- L-BANNER ============================================== -->

	<div class="l-banner-wrap wrapper" role="banner">
		<div class="l-container">
			
			<!-- L-BANNER ============== -->
			<div class="l-banner">
					<?php if (!empty($page['banner'])): ?> 
					<?php print render($page['banner']) ?>
					<?php endif; ?>
					
			</div><!-- end .l-banner -->
		</div><!-- end .l-container -->	
		
	</div> <!-- end .l-banner-wrap -->

<!-- L-PREFIX ============================================== -->
	
	<div class="l-prefix-wrap wrapper">
			<div class="l-container">		
				<div class="l-prefix">
				
					<!-- L-PREFIX-FIRST ============== -->
					<div class="l-prefix-first">
							<?php if (!empty($page['prefix_first'])): ?>
							<?php print render($page['prefix_first']); ?>
							<?php endif; ?>
					</div><!-- end .l-prefix-first -->
				
					<!-- L-PREFIX-SECOND ============== -->
					<div class="l-prefix-second">
							<?php if (!empty($page['prefix_second'])): ?>
							<?php print render($page['prefix_second']); ?>
							<?php endif; ?>
					</div><!-- end .l-prefix-second -->
				
					<!-- L-PREFIX-THIRD ============== -->
					<div class="l-prefix-third">
							<?php if (!empty($page['prefix_third'])): ?>
							<?php print render($page['prefix_third']); ?>
							<?php endif; ?>
					</div><!-- end .l-prefix-third -->
				
				</div><!-- end .l-footer -->
			</div><!-- end .l-container -->
	</div><!-- end .l-footer-wrap -->


<!-- L-MAIN_CONTENT ============================================== -->
	
	<div class="l-main-wrap wrapper">
		<div class="l-container">
		
			<!-- L-CONTENT ============== -->
			<div class="l-content">
			<?php print render($tabs); ?>
			<?php //print $breadcrumb; ?>
			<?php print $messages; ?>
			<?php print render($page['help']); ?>
			
			<!-- Action Links -->
			<?php if ($action_links): ?>
		    <ul class="action-links"><?php print render($action_links); ?></ul>
			<?php endif; ?>
			
			<?php print render($page['content']); ?>
			
			</div><!-- end .l-content -->
			
			<!-- L-SIDEBAR-FIRST ============== -->
			<div class="l-sidebar-first">
			
				<?php print render($page['sidebar_first']); ?>
			
			</div><!-- end .l-sidebar-first -->

		</div><!-- end .l-container -->
	</div><!-- end .l-main-wrap -->
	
</div>

<!-- L-FOOTER ============================================== -->

<footer class="l-footer-wrap wrapper" role="contentinfo">
		<div class="l-container">		
			<div class="l-footer">
			
				<?php if (!empty($page['footer_first'])): ?>
				<div class="l-footer-first">
						
						<?php print render($page['footer_first']); ?>
						
				</div><!-- end .l-footer-first -->
				<?php endif; ?>
				
				<?php if (!empty($page['footer_second'])): ?>
				<div class="l-footer-second">
						
						<?php print render($page['footer_second']); ?>
						
				</div><!-- end .l-footer-second -->
				<?php endif; ?>
				
				<?php if (!empty($page['footer_third'])): ?>
				<div class="l-footer-third">						
						<?php print render($page['footer_third']); ?>						
				</div><!-- end .l-footer-third -->
				<?php endif; ?>
				
			</div><!-- end .l-footer -->
		</div><!-- end .l-container -->
</footer><!-- end .l-footer-wrap -->
</div><!-- end .l-page -->
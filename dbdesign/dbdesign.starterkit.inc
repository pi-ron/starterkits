name = Db Design
description = Comes with a well organized Sass setup based on ohm theme with heavy use of partials.
screenshot = screenshot.png
engine = phptemplate
core = 7.x

; Styles
stylesheets[all][] = css/{{ THEME }}.normalize.css
stylesheets[all][] = css/{{ THEME }}.hacks.css
stylesheets[all][] = css/{{ THEME }}.styles.css

; Regions
regions[navigation]     = Navigation bar
regions[header]         = Header
regions[branding]       = Branding
regions[titlebox]    	= Title Box
regions[ctabox]    	= CTA Box
regions[help]           = Help
regions[content]        = Content
regions[sidebar_first]  = First sidebar
regions[footer_prefix_first]   = Footer Prefix First
regions[footer_prefix_second]  = Footer Prefix Second
regions[footer_prefix_third]   = Footer Prefix Third
regions[footer_first]          = Footer First
regions[footer_second]         = Footer Second

; Scripts
scripts[] = js/{{ THEME }}-functions.js
name = Three Column Responsive Layout
description = A 12 column Singularity GS layout with a header, titlebar, three-column content region, 3 column footer prefix and 2 column footer.
preview = preview.png
template = threecol-layout

; Regions
regions[nav]     = Navigation bar
regions[nav_right]         = Nav Right
regions[banner]         = Banner
regions[branding]       = Branding
regions[prefix_first]   = Prefix First
regions[prefix_second]  = Prefix Second
regions[prefix_third]   = Prefix Third
regions[content_header]    = Content Header
regions[titlebox]    	= Title Box
regions[titlebox_right]    	= Title Box Right
regions[help]           = Help
regions[content_first]        = Content First
regions[content_second]        = Content Second
regions[content_footer]        = Content Footer
regions[sidebar_first]  = First sidebar
regions[footer_first]          = Footer First
regions[footer_second]         = Footer Second
regions[footer_strapline]         = Footer Strapline

; Stylesheets
stylesheets[all][] = css/layouts/threecol/threecol-layout.css
stylesheets[all][] = css/layouts/threecol/threecol-layout.no-query.css
name = Generic v0.01
description = Comes with a well organized Sass setup based on ohm theme with heavy use of partials.
screenshot = screenshot.png
engine = phptemplate
core = 7.x
version = 0.01

; Styles
stylesheets[all][] = css/{{ THEME }}.normalize.css
stylesheets[all][] = css/{{ THEME }}.hacks.css
stylesheets[all][] = css/{{ THEME }}.styles.css
stylesheets[all][] = css/{{ THEME }}.no-query.css

; Regions
regions[nav]     = Navigation bar
regions[nav_right]         = Nav Right
regions[banner]         = Banner
regions[branding]       = Branding
regions[prefix_first]   = Prefix First
regions[prefix_second]  = Prefix Second
regions[prefix_third]   = Prefix Third
regions[content_header]    = Content Header
regions[titlebox]    	= Title Box
regions[titlebox_right]    	= Title Box Right
regions[help]           = Help
regions[content_first]        = Content First
regions[content_second]        = Content Second
regions[content_footer]        = Content Footer
regions[sidebar_first]  = First sidebar
regions[footer_first]          = Footer First
regions[footer_second]         = Footer Second
regions[footer_strapline]         = Footer Strapline

; Scripts
scripts[] = js/{{ THEME }}-functions.js